import React, { Component } from "react";
import Sidebar from "./Sidebar";
import "./common/style/App.css";
import { Document, Page } from "react-pdf/dist/entry.webpack";
import ConvertAndUpload from "./common/ConvertAndUpload";

export default class PdfSplitScreenViewer extends Component<Props, State> {
  state = {
    file: "",
    files: [],
    pageNumber: 1
  };
  getFiles(files) {
    this.setState({ files: files });
  }

  state: State;

  goToPrevPage = () =>
    this.setState(state => ({ pageNumber: state.pageNumber - 1 }));
  goToNextPage = () =>
    this.setState(state => ({ pageNumber: state.pageNumber + 1 }));

  onFileChange = event => {
    this.setState({
      file: event.target.files[0]
    });
  };

  render() {
    const { file, pageNumber } = this.state;
    return (
      <div className="App" style={{ display: "flex", height: "100vh" }}>
        <Sidebar />
        <div
          style={{
            height: "100vh",
            width: "600vw",
            overflowY: "scroll",
            position: "relative"
          }}
        >
          <div>
            <input type="file" onChange={this.onFileChange} />
            <nav>
              <button onClick={this.goToPrevPage}>Prev</button>
              <button onClick={this.goToNextPage}>Next</button>
            </nav>
            <ConvertAndUpload
              multiple={true}
              onDone={this.getFiles.bind(this)}
            />
            {this.state.files.length !== 0 ? (
              <div>
                <h3 className="text-center mt-25">Callback Object</h3>
                <div className="pre-container">
                  <pre>{JSON.stringify(this.state.files, null, 2)}</pre>
                </div>
              </div>
            ) : null}
            <Document file={file} onLoadSuccess={this.onDocumentLoadSuccess}>
              <Page pageNumber={pageNumber} width={600} />
            </Document>{" "}
          </div>
        </div>
      </div>
    );
  }
}
