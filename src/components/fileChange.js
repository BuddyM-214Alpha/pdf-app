// import React, { Component } from "react";
// import { connect } from 'react-redux';
//
// export default ChildComponent => {
//   class HigherOrderComponent extends Component {
//
//     ComponentDidMount() {
//       this.ActionHere();
//     }
//     componnentDidUpdate() {
//       this.ActionHere();
//     }
//
//     ActionHere() {
//       // do something
//     }
//     render() {
//       return <ChildComponent {...this.props} />;
//     }
//   }
//
//   function mapStateToProps(state) {
//     return { variable: state.};
//   }
//
//   return connect(mapStateToProps)(HigherOrderComponent);
// };

// In lower Component

import React, { Component } from "react";
import { connect } from "react-redux";
import PdfViewer from "./PdfViewer";

export default FileChange => {
  class HandleChanges extends Component {
    constructor(props) {
      super(props);
      this.state = {
        files: []
      };
    }

    handleChange = event => {
      // get the files
      let files = event.target.files[0];

      // Process each file
      var allFiles = [];
      for (var i = 0; i < files.length; i++) {
        let file = files[i];

        // Make new FileReader
        let reader = new FileReader();

        // Convert the file to base64 text
        reader.readAsDataURL(file);

        // on reader load somthing...
        reader.onload = () => {
          // Make a fileInfo Object
          let fileInfo = {
            name: file.name,
            type: file.type,
            size: Math.round(file.size / 1000) + " kB",
            base64: reader.result,
            file: file
          };

          // Push it to the state
          allFiles.push(fileInfo);

          // If all files have been proceed
          if (allFiles.length === files.length) {
            // Apply Callback function
            if (this.props.multiple) this.props.onDone(allFiles);
            else this.props.onDone(allFiles[0]);
          }
        }; // reader.onload
      } // for
    };

    onFileChange = event => {
      this.setState({
        file: event.target.files[0]
      });
    };
  }

  HandleChanges.defaultProps = {
    multiple: false
  };

  const mapStateToProps = state => {
    return { props: state.props };
  };
  return connect(HandleChanges);
};

//  in Main or App or whatever

// import FileChange from "./path-to-LowerComponent";
