import React from "react";
import PdfViewer from "./PdfViewer";

class Main extends React.Component {
  render() {
    return <PdfViewer />;
  }
}

export default Main;
