// @flow

import React from "react";

function Sidebar() {
  return (
    <div className="sidebar" style={{ width: "250vw" }}>
      <div className="description" style={{ padding: "1rem" }}>
        <h2 style={{ marginBottom: "1rem" }}>Notes</h2>
      </div>
    </div>
  );
}

export default Sidebar;
